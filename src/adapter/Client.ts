import { Adaptee } from "./Adaptee";
import { Adapter } from "./Adapter";

export class Client {
  private adapter: Adapter;

  constructor() {
    const adaptee: Adaptee = new Adaptee();
    this.adapter = new Adapter(adaptee);
  }

  public requestClient() {
    console.log(this.adapter.request());
  }
}
