/**
 * The Target defines the domain-specific interface used by the client code.
 */
export interface IClientInterface {
  request(): string;
}
