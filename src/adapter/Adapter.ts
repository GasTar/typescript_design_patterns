import { Adaptee } from "./Adaptee";
import { IClientInterface } from "./IClientInterface";

/**
 * The Adapter makes the Adaptee's interface compatible with the Target's
 * interface.
 */
export class Adapter implements IClientInterface {
  private adaptee: Adaptee;

  constructor(adaptee: Adaptee) {
    this.adaptee = adaptee;
  }

  public request(): string {
    const result = this.adaptee.specificRequest().split("").reverse().join("");
    return `Adapter: (TRANSLATED) ${result}`;
  }
}
