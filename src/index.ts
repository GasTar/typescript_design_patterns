console.clear();
require("./factoryMethod");
require("./builder");
require("./adapter");
require("./composite");
require("./observer");
require("./state");
require("./strategy");
require("./templateMethod");

console.log();
