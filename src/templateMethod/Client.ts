import { AbstractClass } from "./AbstractClass";

export class Client {
  concreteClass: AbstractClass;

  constructor(concreteClass: AbstractClass) {
    this.concreteClass = concreteClass;
  }

  someOperation() {
    this.concreteClass.templateMethod();
  }

  setConcreteClass(concrete: AbstractClass) {
    this.concreteClass = concrete;
  }
}
