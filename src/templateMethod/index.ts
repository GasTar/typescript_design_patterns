import { Client } from "./Client";
import { ConcreteClass1, ConcreteClass2 } from "./ConcreteClass";

const concreteClass1: ConcreteClass1 = new ConcreteClass1();
const concreteClass2: ConcreteClass2 = new ConcreteClass2();

const client: Client = new Client(concreteClass1);

console.log("Same client code can work with different subclasses:");
client.someOperation();

console.log();

client.setConcreteClass(concreteClass2);

console.log("Same client code can work with different subclasses:");
client.someOperation();
