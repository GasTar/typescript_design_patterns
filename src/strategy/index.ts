import { ConcreteStrategyA, ConcreteStrategyB } from "./ConcreteStrategy";
import { Context } from "./Context";

const concreteStrategyA = new ConcreteStrategyA();
const concreteStrategyB = new ConcreteStrategyB();

const context = new Context(concreteStrategyA);

console.log("Client: Strategy is set to normal sorting.");
context.doSomeBusinessLogic();

console.log();

console.log("Client: Strategy is set to reverse sorting.");
context.setStrategy(concreteStrategyB);
context.doSomeBusinessLogic();
