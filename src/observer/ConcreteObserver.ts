import { ConcreteSubject } from "./ConcreteSubject";
import { IObserver } from "./IObserver";
import { ISubject } from "./ISubject";

/**
 * Concrete Observers react to the updates issued by the Subject they had been
 * attached to.
 */
export class ConcreteObserverA implements IObserver {
  public update(subject: ISubject): void {
    if (subject instanceof ConcreteSubject && subject.state < 3) {
      console.log("ConcreteObserverA: Reacted to the event.");
    }
  }
}

export class ConcreteObserverB implements IObserver {
  public update(subject: ISubject): void {
    if (
      subject instanceof ConcreteSubject &&
      (subject.state === 0 || subject.state >= 2)
    ) {
      console.log("ConcreteObserverB: Reacted to the event.");
    }
  }
}
