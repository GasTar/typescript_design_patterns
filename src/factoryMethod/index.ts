import { Client } from "./Client";
import { ConcreteCreator1, ConcreteCreator2 } from "./ConcreteCreators";

const concreteCreator1: ConcreteCreator1 = new ConcreteCreator1();
const concreteCreator2: ConcreteCreator2 = new ConcreteCreator1();

const client: Client = new Client(concreteCreator1);
client.someOperation();

client.setCreator(concreteCreator2);
client.someOperation();

console.log();
