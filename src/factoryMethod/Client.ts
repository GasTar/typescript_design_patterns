import { Creator } from "./AbstractCreator";

/**
 * The client code works with an instance of a concrete creator, albeit through
 * its base interface. As long as the client keeps working with the creator via
 * the base interface, you can pass it any creator's subclass.
 */
export class Client {
  creator: Creator;

  constructor(creator: Creator) {
    this.creator = creator;
  }

  someOperation() {
    console.log(this.creator.oneOperation());
  }

  setCreator(creator: Creator) {
    this.creator = creator;
  }
}
