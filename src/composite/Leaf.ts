import { IComponent } from "./IComponent";

export class Leaf implements IComponent {
  public operation(): string {
    return "Leaf";
  }
}
