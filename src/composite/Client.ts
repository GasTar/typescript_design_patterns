import { Composite } from "./Composite";
import { Leaf } from "./Leaf";

export class Client {
  compose() {
    const simple = new Leaf();
    console.log("Client: I've got a simple component:");
    console.log(`RESULT: ${simple.operation()}`);
    console.log("");

    const tree = new Composite();
    const branch1 = new Composite();
    branch1.add(new Leaf());
    branch1.add(new Leaf());
    const branch2 = new Composite();
    branch2.add(new Leaf());
    tree.add(branch1);
    tree.add(branch2);
    console.log("Client: Now I've got a composite tree:");
    console.log(`RESULT: ${tree.operation()}`);
    console.log("");
  }
}
