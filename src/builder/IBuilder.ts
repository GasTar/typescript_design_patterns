/**
 * The Builder interface specifies methods for creating the different parts of
 * the Product objects.
 */
export interface IBuilder {
  producePartA(): void;
  producePartB(): void;
  producePartC(): void;
}
